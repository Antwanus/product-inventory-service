This app provides inventory data for product-service.

----------------------------------------------------------------
This app requires ArtemisMQ for JMS.

docker run -it --rm -p 8161:8161 -p 61616:61616 vromero/activemq-artemis

DEFAULT CREDS
    login: artemis
    pw: simetraehcapa
-----------------------------------------------------------------
This app requires EurekaServer

product-services-eureka
port 8761
-----------------------------------------------------------------
This app requires Zipkin for distributed tracing

docker run -d -p 9411:9411 openzipkin/zipkin