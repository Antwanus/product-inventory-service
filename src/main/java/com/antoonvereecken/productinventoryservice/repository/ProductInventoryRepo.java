package com.antoonvereecken.productinventoryservice.repository;

import com.antoonvereecken.productinventoryservice.domain.ProductInventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ProductInventoryRepo extends JpaRepository<ProductInventory, UUID> {

    List<ProductInventory> findAllByProductId(UUID productId);

    List<ProductInventory> findAllByEanId(String eanId);

}
