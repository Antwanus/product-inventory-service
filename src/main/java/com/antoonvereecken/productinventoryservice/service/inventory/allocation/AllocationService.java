package com.antoonvereecken.productinventoryservice.service.inventory.allocation;

import com.antoonvereecken.common.model.ProductOrderDto;

public interface AllocationService {

    Boolean allocateOrder(ProductOrderDto orderDto);

}
