package com.antoonvereecken.productinventoryservice.service.inventory;

import com.antoonvereecken.productinventoryservice.domain.ProductInventory;
import com.antoonvereecken.productinventoryservice.repository.ProductInventoryRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Service
public class InventoryServiceImpl implements InventoryService {

    private final ProductInventoryRepo repo;

    @Override
    public ProductInventory findInventoryByInventoryId(UUID inventoryId) {
        return repo.getOne(inventoryId);
    }


}