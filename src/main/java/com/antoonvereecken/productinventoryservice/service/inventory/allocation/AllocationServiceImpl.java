package com.antoonvereecken.productinventoryservice.service.inventory.allocation;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.common.model.ProductOrderLineDto;
import com.antoonvereecken.productinventoryservice.domain.ProductInventory;
import com.antoonvereecken.productinventoryservice.repository.ProductInventoryRepo;
import com.antoonvereecken.productinventoryservice.web.mapper.ProductInventoryMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RequiredArgsConstructor
@Service
public class AllocationServiceImpl implements AllocationService {

    private final ProductInventoryRepo inventoryRepo;

    @Override
    public Boolean allocateOrder(ProductOrderDto orderDto) {
        log.debug("ALLOCATING ORDER ---> " + orderDto.getId());

        AtomicInteger totalOrdered = new AtomicInteger();
        AtomicInteger totalAllocated = new AtomicInteger();

        List<ProductOrderLineDto> orderLines = orderDto.getProductOrderLines();

        for(ProductOrderLineDto orderLine : orderLines) {
            int orderQty = (orderLine.getOrderQuantity() == null) ? 0 : orderLine.getOrderQuantity();
            int allocatedQty = (orderLine.getAllocatedQuantity() == null) ? 0 : orderLine.getAllocatedQuantity();
            int qtyToBeAllocated = orderQty - allocatedQty;

            if (qtyToBeAllocated > 0) {
                allocateOrderLine(orderLine);
            }
            totalOrdered.set(totalOrdered.getAndAdd(orderLine.getOrderQuantity()));
            totalAllocated.set(totalAllocated.getAndAdd(
                    (orderLine.getAllocatedQuantity() != null) ? orderLine.getAllocatedQuantity() : 0)
            );

        }

        return totalOrdered.get() == totalAllocated.get();
    }

    private void allocateOrderLine(ProductOrderLineDto orderLine) {
        List<ProductInventory> productInventoryList = inventoryRepo.findAllByEanId(orderLine.getEanId());

        productInventoryList.forEach(productInventory ->  {
            int inventoryQty = (productInventory.getCurrentStock() == null) ? 0 : productInventory.getCurrentStock();
            int orderQty = (orderLine.getOrderQuantity() == null) ? 0 : orderLine.getOrderQuantity();
            int allocatedQty = (orderLine.getAllocatedQuantity() == null) ? 0 : orderLine.getAllocatedQuantity();
            int qtyToBeAllocated = orderQty - allocatedQty;

            if (inventoryQty >= qtyToBeAllocated) { // full allocation
                productInventory.setCurrentStock(inventoryQty - qtyToBeAllocated);
                orderLine.setAllocatedQuantity(orderQty);

                inventoryRepo.save(productInventory);
            } else if (inventoryQty > 0) { // partial allocation
                productInventory.setCurrentStock(0);
                orderLine.setAllocatedQuantity(allocatedQty + inventoryQty);

            }
            // doublechecking the currentStock is now 0 -> delete the record
            if (productInventory.getCurrentStock() == 0) {
                inventoryRepo.delete(productInventory);
            }
        });


    }
}
