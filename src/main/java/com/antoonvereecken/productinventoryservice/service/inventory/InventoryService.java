package com.antoonvereecken.productinventoryservice.service.inventory;

import com.antoonvereecken.productinventoryservice.domain.ProductInventory;

import java.util.UUID;

public interface InventoryService {

    ProductInventory findInventoryByInventoryId(UUID inventoryId);

}
