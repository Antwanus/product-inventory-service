package com.antoonvereecken.productinventoryservice.service.inventory.deallocation;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.productinventoryservice.domain.ProductInventory;
import com.antoonvereecken.productinventoryservice.repository.ProductInventoryRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class DeallocationServiceImpl implements DeallocationService {

    private final ProductInventoryRepo invRepo;

    @Override
    public void deallocateOrder(ProductOrderDto productOrderDto) {

        productOrderDto.getProductOrderLines().forEach(line -> {
            ProductInventory inv = ProductInventory.builder()
                    .productId(line.getProductId())
                    .eanId(line.getEanId())
                    .currentStock(line.getAllocatedQuantity())
                    .build();

            ProductInventory savedInv = invRepo.save(inv);

            log.debug("DEALLOCATION SAVED FOR EAN: " + savedInv.getEanId() + ", INV_ID: " + savedInv.getInventoryId());

        });

    }
}
