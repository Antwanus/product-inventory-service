package com.antoonvereecken.productinventoryservice.service.inventory.deallocation;

import com.antoonvereecken.common.model.ProductOrderDto;

public interface DeallocationService {

    void deallocateOrder(ProductOrderDto productOrderDto);


}
