package com.antoonvereecken.productinventoryservice.service.listener;

import com.antoonvereecken.common.event.DeallocateOrderRequest;
import com.antoonvereecken.productinventoryservice.config.JmsConfig;
import com.antoonvereecken.productinventoryservice.service.inventory.deallocation.DeallocationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class DeallocationListener {

    private final DeallocationService deallocationService;

    @JmsListener(destination = JmsConfig.DEALLOCATE_ORDER_QUEUE)
    void listen(DeallocateOrderRequest request) {
        deallocationService.deallocateOrder(request.getProductOrderDto());
    }


}
