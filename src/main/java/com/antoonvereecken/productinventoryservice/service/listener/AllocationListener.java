package com.antoonvereecken.productinventoryservice.service.listener;

import com.antoonvereecken.common.event.AllocateOrderRequest;
import com.antoonvereecken.common.event.AllocateOrderResult;
import com.antoonvereecken.productinventoryservice.config.JmsConfig;
import com.antoonvereecken.productinventoryservice.service.inventory.allocation.AllocationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class AllocationListener {

    private final AllocationService allocationService;
    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.ALLOCATE_ORDER_QUEUE)
    public void listen(AllocateOrderRequest allocateOrderRequest) {
        AllocateOrderResult.AllocateOrderResultBuilder allocateOrderResultBuilder = AllocateOrderResult.builder();
        allocateOrderResultBuilder.productOrderDto(allocateOrderRequest.getProductOrderDto());

        try {
            Boolean isAllocationResultSuccess = allocationService.allocateOrder(allocateOrderRequest.getProductOrderDto());

            allocateOrderResultBuilder.pendingInventory( !Boolean.TRUE.equals(isAllocationResultSuccess) );

            allocateOrderResultBuilder.allocationError(false);
        } catch (Exception e) {
            log.error("ALLOCATION ((( FAILED ))) ORDER_ID: " + allocateOrderRequest.getProductOrderDto().getId());
            allocateOrderResultBuilder.allocationError(true);
        }

        jmsTemplate.convertAndSend(JmsConfig.ALLOCATE_ORDER_RESULT_QUEUE, allocateOrderResultBuilder.build());
    }


}
