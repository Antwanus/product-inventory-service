package com.antoonvereecken.productinventoryservice.service.listener;

import com.antoonvereecken.common.event.NewInventoryEvent;
import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.productinventoryservice.config.JmsConfig;
import com.antoonvereecken.productinventoryservice.domain.ProductInventory;
import com.antoonvereecken.productinventoryservice.repository.ProductInventoryRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class NewInventoryListener {

    private final ProductInventoryRepo inventoryRepo;

    @JmsListener(destination = JmsConfig.NEW_INVENTORY_QUEUE)
    public void listen(NewInventoryEvent event) {
        log.debug("NewInventoryListener.listen() |>|>|> new-inventory event on queue: " + event.toString());

        ProductDto dto = event.getProductDto();
        ProductInventory inventory = ProductInventory.builder()
                .productId(dto.getId())
                .eanId(dto.getEanId())
                .currentStock(dto.getCurrentStock())
                .build();

        inventoryRepo.save(inventory);
    }

}
