package com.antoonvereecken.productinventoryservice.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class ProductInventory extends BaseEntity {

    @Type(type="org.hibernate.type.UUIDCharType")
    private UUID productId;

    @Column(length = 13, columnDefinition = "varchar(13)")
    private String eanId;

    private Integer currentStock = 50;

    @Builder
    public ProductInventory(
            UUID id,
            Long version,
            Timestamp createdDate,
            Timestamp lastModifiedDate,
            UUID productId,
            String eanId,
            Integer currentStock
    ) {
        super(id, version, createdDate, lastModifiedDate);
        this.productId = productId;
        this.eanId = eanId;
        this.currentStock = currentStock;
    }


}
