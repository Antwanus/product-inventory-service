package com.antoonvereecken.productinventoryservice.web.mapper;

import com.antoonvereecken.common.model.ProductInventoryDto;
import com.antoonvereecken.productinventoryservice.domain.ProductInventory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = { DateMapper.class })
public interface ProductInventoryMapper {

    @Mapping(target = "eanId", ignore = true)
    ProductInventory dtoToProductInventory(ProductInventoryDto dto);

    ProductInventoryDto productInventoryToDto(ProductInventory productInventory);

}
