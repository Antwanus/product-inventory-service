package com.antoonvereecken.productinventoryservice.web.controller;

import com.antoonvereecken.common.model.ProductInventoryDto;
import com.antoonvereecken.productinventoryservice.repository.ProductInventoryRepo;
import com.antoonvereecken.productinventoryservice.web.mapper.ProductInventoryMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@RestController
public class ProductInventoryController {

    private final ProductInventoryRepo repo;
    private final ProductInventoryMapper mapper;

    @GetMapping("api/v1/product/{productId}/inventory")
    public List<ProductInventoryDto> listProductInventoryByProductId(@PathVariable("productId") UUID productId) {
        log.info("Finding inventory for productId ---> " + productId);

        return repo.findAllByProductId(productId)
                .stream()
                .map(mapper::productInventoryToDto)
                .collect(Collectors.toList());
    }

}
