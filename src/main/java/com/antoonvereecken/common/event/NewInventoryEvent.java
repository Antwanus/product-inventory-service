package com.antoonvereecken.common.event;

import com.antoonvereecken.common.model.ProductDto;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class NewInventoryEvent extends ProductEvent {

    public NewInventoryEvent(ProductDto productDto) {
        super(productDto);
    }
}
