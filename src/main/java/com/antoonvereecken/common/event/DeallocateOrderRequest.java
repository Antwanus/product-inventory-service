package com.antoonvereecken.common.event;

import com.antoonvereecken.common.model.ProductOrderDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeallocateOrderRequest {

    private ProductOrderDto productOrderDto;


}
