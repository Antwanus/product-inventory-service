package com.antoonvereecken.common.event;

import com.antoonvereecken.common.model.ProductOrderDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AllocateOrderResult {

    private ProductOrderDto productOrderDto;
    private Boolean allocationError = false;
    private Boolean pendingInventory = false;


}
