package com.antoonvereecken.common.event;

import com.antoonvereecken.common.model.ProductDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductEvent implements Serializable {

    static final long serialVersionUID = 8461860887724625017L;

    private ProductDto productDto;

}
