package com.antoonvereecken.productinventoryservice.service.inventory;

import com.antoonvereecken.productinventoryservice.domain.ProductInventory;
import com.antoonvereecken.productinventoryservice.repository.ProductInventoryRepo;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InventoryServiceImplTest {

    @Mock ProductInventoryRepo repo;
    @InjectMocks InventoryServiceImpl service;
    ProductInventory inv;
    UUID id;

    @BeforeEach
    void createTestObjectsBeforeTest() {
        inv = ProductInventory.builder()
                .id(UUID.randomUUID())
                .eanId("123456789")
                .currentStock(1337)
                .build();
        id = inv.getInventoryId();
        repo.save(inv);
    }
    @AfterEach
    void deleteTestObjectAfterTest() {
        repo.delete(inv);
        id = null;
    }

    @Test
    void testFindProductInventoryByProductId() {
        when(repo.getOne(id)).thenReturn(inv);
        ProductInventory found = service.findInventoryByInventoryId(id);
        verify(repo, atMost(1)).getOne(id);
        verify(repo, never()).findById(id);
        assertThat(found).isNotNull();

    }
}