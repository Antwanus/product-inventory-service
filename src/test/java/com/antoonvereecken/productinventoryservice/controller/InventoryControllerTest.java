package com.antoonvereecken.productinventoryservice.controller;

import com.antoonvereecken.common.model.ProductInventoryDto;
import com.antoonvereecken.productinventoryservice.service.inventory.InventoryService;
import com.antoonvereecken.productinventoryservice.web.controller.ProductInventoryController;
import com.antoonvereecken.productinventoryservice.web.mapper.ProductInventoryMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.OffsetDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

class InventoryControllerTest {

    @Autowired ObjectMapper objectMapper;
    @MockBean InventoryService inventoryService;

    ProductInventoryMapper inventoryMapper;

    ProductInventoryDto inventoryDto;
    String inventoryDtoJsonString;

    @BeforeEach
    void createTestInventoryDto() {
        this.inventoryDto = ProductInventoryDto.builder()
                .inventoryId(UUID.randomUUID())
                .productId(UUID.randomUUID())
                .createdDate(OffsetDateTime.now())
                .lastModifiedDate(OffsetDateTime.now())
                .currentStock(123)
                .build();

    }


}
